package com.magenic.demo.pagemodels;

import com.magenic.jmaqs.selenium.baseSeleniumTest.SeleniumTestObject;
import org.openqa.selenium.By;

public class AboutPageModel extends BasePageModel{

  private static final By EMAIL_ADDRESS = By.cssSelector("#AboutTable > tbody > tr:nth-child(1) > td:nth-child(2) > a");
  private static final By PHONE_NUMBER = By.cssSelector("#AboutTable > tbody > tr:nth-child(2) > td:nth-child(2) > a");

  /**
   * Instantiates a new Base page model.
   *
   * @param testObject the test object
   */
  public AboutPageModel(SeleniumTestObject testObject) {
    super(testObject);
  }

  public String getPhoneNumber() {
    return this.testObject.getSeleniumWait().waitForVisibleElement(PHONE_NUMBER).getText();
  }

  public String getEmailAddress() {
    return this.testObject.getSeleniumWait().waitForVisibleElement(EMAIL_ADDRESS).getText();
  }

  @Override
  public boolean isPageLoaded() {
    return false;
  }
}
