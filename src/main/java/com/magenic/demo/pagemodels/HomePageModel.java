package com.magenic.demo.pagemodels;

import com.magenic.jmaqs.selenium.baseSeleniumTest.SeleniumConfig;
import com.magenic.jmaqs.selenium.baseSeleniumTest.SeleniumTestObject;
import org.openqa.selenium.By;

/**
 * The type Home page model.
 */
public class HomePageModel extends BasePageModel {

  /**
   * The URL for the page.
   */
  private static final String PAGE_URL = SeleniumConfig.getWebSiteBase()
      + "Static/Training3/HomePage.html";

  /**
   * Welcome Message Selector.
   */
  private static final By WELCOME_MESSAGE = By.cssSelector("#WelcomeMessage");


  private static final By ABOUT_TAB = By.cssSelector("#About");

  /**
   * Instantiates a new Home page model.
   *
   * @param testObject
   *          the test object
   */
  public HomePageModel(SeleniumTestObject testObject) {
    super(testObject);
  }

  /**
   * Check if home page has been loaded
   * 
   * @return True if the page was loaded
   */
  public boolean isPageLoaded() {
    return this.testObject.getSeleniumWait().waitForVisibleElement(WELCOME_MESSAGE).isDisplayed();
  }

  public AboutPageModel clickAboutTab()
  {
      this.testObject.getSeleniumWait().waitForClickableElement(ABOUT_TAB).click();
      return new AboutPageModel(this.testObject);
  }
}
