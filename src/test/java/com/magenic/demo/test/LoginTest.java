package com.magenic.demo.test;

import com.magenic.demo.pagemodels.AboutPageModel;
import com.magenic.jmaqs.selenium.baseSeleniumTest.BaseSeleniumTest;
import com.magenic.demo.pagemodels.HomePageModel;
import com.magenic.demo.pagemodels.LoginPageModel;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class LoginTest extends BaseSeleniumTest {

  @Test
  public void OpenPageTest() {
    LoginPageModel page = new LoginPageModel(this.getSeleniumTestObject());
    page.openLoginPage();
  }

  @Test
  public void enterValidCredentialsTest() {
    String username = "Ted";
    String password = "123";
    LoginPageModel page = new LoginPageModel(this.getSeleniumTestObject());
    page.openLoginPage();
    HomePageModel homepage = page.loginWithValidCredentials(username, password);
    Assert.assertTrue(homepage.isPageLoaded());
  }

  @Test
  public void EnterInvalidCredentials() {
    String username = "NOT";
    String password = "Valid";
    LoginPageModel page = new LoginPageModel(this.getSeleniumTestObject());
    page.openLoginPage();
    Assert.assertTrue(page.loginWithInvalidCredentials(username, password));
  }

  @Test
  public void validateAboutTab(){
    String username = "Ted";
    String password = "123";
    LoginPageModel page = new LoginPageModel(this.getSeleniumTestObject());
    page.openLoginPage();
    HomePageModel homepage = page.loginWithValidCredentials(username, password);
    AboutPageModel aboutPageModel = homepage.clickAboutTab();
    SoftAssert softAssert = new SoftAssert();
    softAssert.assertEquals(aboutPageModel.getEmailAddress(), "FakeSample@faker.fake", "Validating the email address.");
    softAssert.assertEquals(aboutPageModel.getPhoneNumber(), "+1 (555) 555-5555", "Validating the phone number.");
    softAssert.assertAll();
  }
}